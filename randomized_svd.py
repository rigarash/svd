"""
Randomized SVD originally from scikit-learn
"""
# Authors: Gael Varoquaux
#          Alexandre Gramfort
#          Alexandre T. Passos
#          Olivier Grisel
#          Lars Buitinck
#          Stefan van der Walt
#          Kyle Kastner
#          Ryo IGARASHI
# License: BSD 3 clause

from __future__ import division
import numbers

import numpy as np
from scipy import linalg
from scipy import fftpack

def check_random_state(seed):
    """Turn seed into a np.random.RandomState instance

    If seed is None, return the RandomState singleton used by np.random.
    If seed is an int, return a new RandomState instance seeded with seed.
    If seed is already a RandomState instance, return it.
    Otherwise raise ValueError.
    """
    if seed is None or seed is np.random:
        return np.random.mtrand._rand
    if isinstance(seed, (numbers.Integral, np.integer)):
        return np.random.RandomState(seed)
    if isinstance(seed, np.random.RandomState):
        return seed
    raise ValueError('%r cannot be used to seed a numpy.random.RandomState'
                     ' instance' % seed)

def randomized_range_finder_subspace(A, size, n_iter=-1, random_state=None):
    """Computes an orthonormal matrix whose range approximates the range of A.

    Parameters
    ----------
    A: 2D array (m x n)
        The input data matrix
    size: integer
        Size of the return array
    n_iter: integer (-1 by default)
        Number of power iterations used to stabilize the result
        if n_iter < 0, calculate as accurate as possible (log(min(m, n)))
        log(min(m, n)) corresponds to almost maximal accuracy.
    random_state: RandomState or an int seed (0 by default)
        A random number generator instance

    Returns
    -------
    Q: 2D array
        A (size x size) projection matrix, the range of which
        approximates well the range of the input matrix A.

    Notes
    -----

    Follows Algorithm 4.4(instead of 4.3) of
    Finding structure with randomness: Stochastic algorithms for constructing
    approximate matrix decompositions
    Halko, et al., SIAM Rev. Vol. 53, No. 2, pp.217-288 (2011)
    """
    random_state = check_random_state(random_state)

    if (n_iter < 0):
        n_iter = int(np.log(min(A.shape[0], A.shape[1])))

    # generating random gaussian vectors r with shape: (A.shape[1], size)
    Y = random_state.normal(size=(A.shape[1], size))

    # sampling the range of A using by linear projection of r
    Y = np.dot(A, Y)

    # extracting an orthonormal basis of the A range samples
    Q, _ = linalg.qr(Y, mode='economic')

    # perform power iterations with reorthoganalization to further 'imprint'
    # the to singular vectors of A in stable way
    # compared to Algorithm 4.4, do QR factorization half times.
    for i in range(n_iter):
        Q, _ = linalg.qr(np.dot(A, np.dot(A.T, Q)), mode='economic')

    return Q

def randomized_range_finder_SRFT(A, size, n_iter, random_state=None):
    random_state = check_random_state(random_state)
    A_row, _ = A.shape

    # diagonal matrix with elements equally distributed in sphere
    D = random_state.choice([-1.0, 1.0], size=A_row)

    # truncate FAD by setting n to irfft
    AD  = A * D
    FAD = fftpack.irfft(AD, n=size, overwrite_x=True)

    # extracting an orthonormal basis of the A range samples
    Q, _ = linalg.qr(FAD, mode='economic')
    return Q

def _randomized_svd_calculation(M, n_random, n_iter=-1, random_state=0, range_finder='SRFT'):
    if range_finder == 'SRFT':
        Q = randomized_range_finder_SRFT(M, n_random, n_iter, random_state)
    elif range_finder == 'subspace':
        Q = randomized_range_finder_subspace(M, n_random, n_iter, random_state)

    # project M to the (k + p) dimensional space using the basis vectors
    B = np.dot(Q.T, M)

    # compute the SVD on the thin matrix: (k + p) wide
    Uhat, s, V = linalg.svd(B, full_matrices=False)
    del B
    U = np.dot(Q, Uhat)
    return U, s, V

def randomized_svd(M, n_components, n_oversamples=10, n_iter=-1,
                   transpose='auto', flip_sign=True, random_state=0, range_finder='SRFT'):
    """Computes a truncated randomized SVD

    Parameters
    ----------
    M: ndarray or sparse matrix
        Matrix to decompose

    n_components: int
        Number of singular values and vectors to extract.

    n_oversamples: int (default is 10)
        Additional number of random vectors to sample the range of M so as
        to ensure proper conditioning. The total number of random vectors
        used to find the range of M is n_components + n_oversamples.

    n_iter: int (default is -1)
        Number of power iterations (can be used to deal with very noisy
        problems).

    transpose: True, False or 'auto' (default)
        Whether the algorithm should be applied to M.T instead of M. The
        result should approximately be the same. The 'auto' mode will
        trigger the transposition if M.shape[1] > M.shape[0] since this
        implementation of randomized SVD tend to be a little faster in that
        case).

    flip_sign: boolean, (True by default)
        The output of a singular value decomposition is only unique up to a
        permutation of the signs of the singular vectors. If `flip_sign` is
        set to `True`, the sign ambiguity is resolved by making the largest
        loadings for each component in the left singular vectors positive.

    random_state: RandomState or an int seed (0 by default)
        A random number generator instance to make behavior

    range_finder: 'SRFT' or 'subspace' ('SRFT' by default)
        Range finders are algorithms how the subspace of the matrix is
        calculated. 'SRFT' stands for "Subsampled Random Fourier Transform",
        and it is faster by order in most of the cases.
        'subspace' stands for original Gaussian matrices implementation,
        and it is more stable than others but slower.

    Notes
    -----
    This algorithm finds a (usually very good) approximate truncated
    singular value decomposition using randomization to speed up the
    computations. It is particularly fast on large matrices on which
    you wish to extract only a small number of components.

    References
    ----------
    * Finding structure with randomness: Stochastic algorithms for constructing
      approximate matrix decompositions
      Halko, et al., 2009 http://arxiv.org/abs/arXiv:0909.4061

    * A randomized algorithm for the decomposition of matrices
      Per-Gunnar Martinsson, Vladimir Rokhlin and Mark Tygert
    """
    random_state = check_random_state(random_state)
    n_random = n_components + n_oversamples
    n_samples, n_features = M.shape

    if transpose == 'auto' and n_samples > n_features:
        transpose = True
    if transpose:
        # this implementation is a bit faster with smaller shape[1]
        M = M.T

    U, s, V = _randomized_svd_calculation(M, n_random, n_iter, random_state, range_finder)

    if flip_sign:
        U, V = svd_flip(U, V)

    if transpose:
        # transpose back the results according to the input convention
        return V[:n_components, :].T, s[:n_components], U[:, :n_components].T
    else:
        return U[:, :n_components], s[:n_components], V[:n_components, :]

def svd_flip(u, v, u_based_decision=True):
    """Sign correction to ensure deterministic output from SVD.

    Adjusts the columns of u and the rows of v such that the loadings in the
    columns in u that are largest in absolute value are always positive.

    Parameters
    ----------
    u, v : ndarray
        u and v are the output of `linalg.svd` or
        `sklearn.utils.extmath.randomized_svd`, with matching inner dimensions
        so one can compute `np.dot(u * s, v)`.

    u_based_decision : boolean, (default=True)
        If True, use the columns of u as the basis for sign flipping. Otherwise,
        use the rows of v. The choice of which variable to base the decision on
        is generally algorithm dependent.


    Returns
    -------
    u_adjusted, v_adjusted : arrays with the same dimensions as the input.

    """
    if u_based_decision:
        # columns of u, rows of v
        max_abs_cols = np.argmax(np.abs(u), axis=0)
        signs = np.sign(u[max_abs_cols, range(u.shape[1])])
        u *= signs
        v *= signs[:, np.newaxis]
    else:
        # rows of v, columns of u
        max_abs_rows = np.argmax(np.abs(v), axis=1)
        signs = np.sign(v[range(v.shape[0]), max_abs_rows])
        u *= signs
        v *= signs[:, np.newaxis]
    return u, v
