#!/usr/bin/python3

import time
from randomized_svd import randomized_svd, check_random_state

import numpy as np
import scipy.linalg
import scipy.sparse.linalg

class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000  # millisecs
        if self.verbose:
            print('elapsed time: ', self.msecs, ' ms')

def test(D):
    # For 1/r decay
    #rarray = np.array([(1.0/(i+1)) for i in range(D*D)])
    # For almost all degenerate case
    #rarray = np.array([1.0 for _ in range(D-1)] + [0.5 for _ in range(D+1)] + [0.1 for _ in range(D*(D-2))])
    # For 0.9^r (exponential) decay
    rarray = np.array([0.9**(i+1) for i in range(D*D)])

    rmatrix = np.diag(rarray)
    random_state = check_random_state(1)
    R = random_state.normal(size=(D*D, D*D))
    U, _, VT = scipy.linalg.svd(R)
    rmatrix = np.dot(np.dot(U, rmatrix), VT.T)
    print("Dimension = ", D)
    # with Timer(True) as t1:
    #     print("LAPACK")
    #     _, S1, _ = scipy.linalg.svd(rmatrix, full_matrices=False) 
    # S1 = S1[:D]
    # print("LAPACK", S1)
    with Timer(True) as t2:
       print("ARPACK")
       _, S2, _ = scipy.sparse.linalg.svds(rmatrix, D)
    S2 = S2[::-1]
    print("ARPACK", S2)
    # with Timer(True) as t3:
    #     print("subspace 0 0")
    #     _, S3, _ = randomized_svd(rmatrix, D, range_finder='subspace', n_iter=0, n_oversamples=0)
    # print("subs ", S3)
    # with Timer(True) as t3:
    #     print("subspace 0 10")
    #     _, S3, _ = randomized_svd(rmatrix, D, range_finder='subspace', n_iter=0, n_oversamples=10)
    # print("subs ", S3)
    with Timer(True) as t3:
        print("subspace 0 D")
        _, S3, _ = randomized_svd(rmatrix, D, range_finder='subspace', n_iter=0, n_oversamples=D)
    print("subs ", S3)
    # with Timer(True) as t3:
    #     print("subspace default 0")
    #     _, S3, _ = randomized_svd(rmatrix, D, range_finder='subspace', n_oversamples=0)
    # print("subs ", S3)
    # with Timer(True) as t3:
    #     print("subspace default 10")
    #     _, S3, _ = randomized_svd(rmatrix, D, range_finder='subspace', n_oversamples=10)
    # print("subs ", S3)
    # with Timer(True) as t3:
    #     print("subspace default D")
    #     _, S3, _ = randomized_svd(rmatrix, D, range_finder='subspace', n_oversamples=D)
    # print("subs ", S3)
    with Timer(True) as t4:
        print("SRFT D")
        _, S4, _ = randomized_svd(rmatrix, D, range_finder='SRFT', n_oversamples=D)
    print("SRFT ", S4)
    # with Timer(True) as t4:
    #     print("SRFT 2D")
    #     _, S4, _ = randomized_svd(rmatrix, D, range_finder='SRFT', n_oversamples=2*D)
    # print("SRFT ", S4)
    # print("Closeness of Singular values between subspace and ARPACK: ", np.allclose(S3, S2, atol=1e-14))
    # print("Closeness of Singular values between subspace and SRFT:   ", np.allclose(S3, S4, atol=1e-14))


#test(10)
#test(20)
#test(30)
#test(40)
#test(50)
#test(60)
test(70)
#test(100)
